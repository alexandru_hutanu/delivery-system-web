<?php

require_once __DIR__
    . DIRECTORY_SEPARATOR
    . '..'
    . DIRECTORY_SEPARATOR
    . 'vendor'
    . DIRECTORY_SEPARATOR
    . 'autoload.php'
;

use Silex\Application;
use Silex\Provider\ServiceControllerServiceProvider;
use Symfony\Component\Form\Forms;
use Symfony\Component\Form\Extension\HttpFoundation\HttpFoundationExtension;
use Symfony\Bridge\Twig\Extension\FormExtension;
use Symfony\Bridge\Twig\Form\TwigRenderer;
use Symfony\Bridge\Twig\Form\TwigRendererEngine;
use Symfony\Bridge\Twig\Extension\TranslationExtension;
use Symfony\Component\Translation\Translator;
use \DeliverySystem\FrontendBundle\Controller\FrontendController;
use \DeliverySystem\APIBundle\Service\TrackingService;
use \DeliverySystem\ConfigurationBundle\Service\ConfigurationService;

/**
 * App Engine for Delivery System WEB
 * @author Alexandru Hutanu <allex.hutanu@gmail.com>
 */

define('DEFAULT_FORM_THEME', 'form_div_layout.html.twig');
define('VENDOR_DIR', realpath(__DIR__ . '/../vendor'));
define('VENDOR_FORM_DIR', VENDOR_DIR . '/symfony/form');
define('VENDOR_TWIG_BRIDGE_DIR', VENDOR_DIR . '/symfony/twig-bridge');
define('VIEWS_DIR', realpath(__DIR__ . '/../src/DeliverySystem/FrontendBundle/Resources/Views'));

$app = new Application();
$app['debug'] = true; //enable debug

/**
 * Register Twig & From components
 */

$twig = new Twig_Environment(new Twig_Loader_Filesystem(array(
    VIEWS_DIR,
    VENDOR_TWIG_BRIDGE_DIR . '/Resources/views/Form',
)));

$formEngine = new TwigRendererEngine(array(DEFAULT_FORM_THEME));
$twig->addExtension(
    new FormExtension(new TwigRenderer($formEngine))
);
$twig->addExtension(
    new TranslationExtension(new Translator('en'))
);
$formEngine->setEnvironment($twig);
// Set up the Form component
$app['form.factory'] = function() {
    return Forms::createFormFactoryBuilder()
        ->addExtension(new HttpFoundationExtension())
        ->getFormFactory();
};

$app['twig'] = function() use ($twig) {
    return $twig;
};

/**
 * Register services
 */

$configService = new ConfigurationService();

$app['tracking.service'] = function () use ($configService) {
    return new TrackingService($configService->getHost());
};


/**
 * Register controllers
 */

//register Controller as a Service provider
$app->register(new ServiceControllerServiceProvider());

//register Acknowledge Controller
$app['frontend.controller'] = function() use ($app) {
    return new FrontendController($app['request_stack']->getCurrentRequest(), $app['form.factory'], $app['twig'], $app['tracking.service']);
};


/**
 * Routes & controllers actions mapping
 */

$app->match('/', "frontend.controller:indexAction")
    ->method('GET|POST');

$app->run();
