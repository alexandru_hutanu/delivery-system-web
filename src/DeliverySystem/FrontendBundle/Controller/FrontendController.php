<?php
namespace DeliverySystem\FrontendBundle\Controller;

use Twig_Environment;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use \DeliverySystem\APIBundle\Service\TrackingService;

/**
 * Class FrontendController
 * @package DeliverySystem\FrontendBundle\Controller
 * @author Alexandru Hutanu <allex.hutanu@gmail.com>
 */
class FrontendController
{
    /** @var FormFactory $formFactoryBuilder */
    private $formFactoryBuilder;

    /** @var Request $request */
    private $request;

    /** @var Twig_Environment $twig */
    private $twig;

    /** @var TrackingService $trackingService */
    private $trackingService;

    /**
    * FrontendController constructor.
    * @param Request $request
    * @param FormFactory $formFactoryBuilder
    * @param Twig_Environment $twig
    * @param TrackingService $trackingService
    */
    public function __construct(
      Request $request,
      FormFactory $formFactoryBuilder,
      Twig_Environment $twig,
      TrackingService $trackingService
    ) {
        $this->request            = $request;
        $this->formFactoryBuilder = $formFactoryBuilder;
        $this->twig               = $twig;
        $this->trackingService    = $trackingService;
    }

    public function indexAction()
    {
        $requestPath = $this->request->getBasePath();
        $form = $this->formFactoryBuilder->createBuilder()
             ->add(
                 'trackingCode',
                 TextType::class,
                 array(
                     'label' => 'Tracking Code',
                     'attr' => array('id' => 'tracking-code')
                 )
             )
             ->add(
                 'send',
                 SubmitType::class,
                 array(
                     'attr' => array( 'class' => 'btn btn-primary btn-sm')
                 )
             )
             ->getForm();

        $form->handleRequest($this->request);

        if ($form->isValid()) {
            $data             = $form->getData();
            $deliveryEstimate = $this->trackingService->getDeliveryEstimateByTrackingCode($data['trackingCode']);
            $error            = $this->checkDeliveryEstimateResponse($deliveryEstimate);

            if ($error) {
                return $this->twig->render(
                    'error.html.twig',
                    array(
                        'error' => $error,
                        'requestPath' => $requestPath, //for assets path
                    )
                );
            }

            return $this->twig->render(
                'delivery.html.twig',
                array(
                  'deliveryEstimate' => $deliveryEstimate['delivery_estimate'],
                  'requestPath' => $requestPath, //for assets path
                )
            );
        }

        return $this->twig->render(
            'frontend.html.twig',
            array(
                'form' => $form->createView(),
                'requestPath' => $requestPath, //for assets path
            )
        );
    }

    /**
     * @param $deliveryEstimate
     * @return null|string
     */
    private function checkDeliveryEstimateResponse($deliveryEstimate)
    {
        if (empty($deliveryEstimate)) {
            return 'Empty API Response. Please check connection with the Backend API.';
        }

        if (isset($deliveryEstimate['error'])) {
            return $deliveryEstimate['error'];
        }

        return null;
    }
}
