<?php
namespace DeliverySystem\APIBundle\Service;

/**
 * Class TrackingService
 * @package DeliverySystem\APIBundle\Service
 * @author Alexandru Hutanu <allex.hutanu@gmail.com>
 */
class TrackingService
{
    /** @var string $host */
    private $host;

    /**
     * TrackingService constructor.
     * @param $host
     */
    public function __construct($host)
    {
        $this->host = $host;
    }

    /**
     * @param $trackingCode
     * @return mixed
     */
    public function getDeliveryEstimateByTrackingCode($trackingCode)
    {
        return json_decode($this->curlGet($this->host . $trackingCode), true);
    }

    /**
     * @param $url
     * @return mixed
     */
    private function curlGet($url)
    {
        $curlHandle = curl_init();

        //set url
        curl_setopt($curlHandle, CURLOPT_URL, $url);

        //catch output
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);

        //send GET
        curl_setopt($curlHandle, CURLOPT_CUSTOMREQUEST, 'GET');

        //grab response in var
        $response = curl_exec($curlHandle);

        //close connection to free up memory
        curl_close($curlHandle);

        //return response
        return $response;
    }
}