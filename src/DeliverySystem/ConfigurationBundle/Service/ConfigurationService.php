<?php
namespace DeliverySystem\ConfigurationBundle\Service;

use Symfony\Component\Yaml\Yaml;

/**
 * Class ConfigurationService
 * @package DeliverySystem\ConfigurationBundle\Service
 * @author Alexandru Hutanu <allex.hutanu@gmail.com>
 */
class ConfigurationService
{
    const CONFIG_YAML_PATH = 'config/config.yml';

    /**
     * @throws \RuntimeException
     */
    public function __construct()
    {
        $currentDir = dirname(__FILE__);
        $pathToYml  = $currentDir .
            DIRECTORY_SEPARATOR . '..' .
            DIRECTORY_SEPARATOR . '..' .
            DIRECTORY_SEPARATOR . '..' .
            DIRECTORY_SEPARATOR . '..' .
            DIRECTORY_SEPARATOR . self::CONFIG_YAML_PATH;
        
        if (!file_exists($pathToYml)) {
            throw new \RuntimeException(sprintf('Cannot find file %s', $pathToYml));
        }

        $yamlConfig           = file_get_contents($pathToYml);
        $configuration        = Yaml::parse($yamlConfig);
        $this->config = $configuration['config'];
    }

    /**
     * @return mixed
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @return string
     */
    public function getHost()
    {
        return $this->config['host'];
    }
}